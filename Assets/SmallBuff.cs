﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SmallBuff : MonoBehaviour {

    public BuffDataType id;
    public Image iconRef;
    // Use this for initialization
    void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void assignID(BuffDataType _id)
    {
        id = _id;
    }

    public void changeSprite(string _name)
    {
        iconRef.sprite = ImageManager.inst.IconSprite.Find(o => o.name == _name);
    }

    public void changeColor(Color _color)
    {
        iconRef.color = _color;
    }

}
