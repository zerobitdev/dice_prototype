﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RedrawSlider : MonoBehaviour,IEndDragHandler {
    Slider slider;
    public Text apText;
	// Use this for initialization
	void Start () {
        slider = GetComponent<Slider>();
        slider.onValueChanged.AddListener(delegate
        {
            ValueChangeCheck();
        });
	}
	
	// Update is called once per frame
	void Update () {

    }

    void ValueChangeCheck()
    {
        if (CardController.inst.cardIsPlaying)
        {
            return;
        }
        if (slider.value == 1)
        {
            CardController.inst.Redraw(GameController.inst.CurrentPlayer);
            slider.value = 0;
        }
        
    }

    public void OnEndDrag(PointerEventData data)
    {
        slider.value = 0;
    }

}
