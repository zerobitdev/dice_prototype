﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CellProperty : MonoBehaviour {
    [Header("Cell ID")]
    public int cellId;
    public int previousCell;
    public int nextCell;
    public int blockType;

    [Header("Cell Position")]
    public Vector3 offsetPosition;
    
    [Header ("Occupied List Ref")]
    public List<Unit> occupiedList = new List<Unit>();

    float x, y, z;
    Vector3 pos;

    // Use this for initialization
    void Start () {
        if (cellId == 0)
        {
            previousCell = BoardController.inst.cellPosition.Length-1;
        }
        else
        {
            previousCell = cellId - 1;
        }

        if (cellId == BoardController.inst.cellPosition.Length-1)
        {
            nextCell = 0;
        }
        else
        {
            nextCell = cellId + 1;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public Vector3 GetFloorPosition()
    {
        x = transform.position.x + offsetPosition.x;
        y = transform.position.y + offsetPosition.y;
        z = transform.position.z + offsetPosition.z;
        pos = new Vector3(x, y, z); ;
        return pos;
    }

    public void AddUnitOnCell(Unit item)
    {
        occupiedList.Add(item);
        SortPiece();
    }

    public void RemoveUnitOnCell(Unit item)
    {
        occupiedList.Remove(item);
        SortPiece();
    }

    public void SortPiece()
    {
        switch (occupiedList.Count)
        {
            case 0:
                break;
            case 1:
                occupiedList[0].transform.position = new Vector3(x, y, z);
                break;
            case 2:
                occupiedList[0].transform.position = new Vector3(x + 0.2f, y, z - 0.2f);
                occupiedList[1].transform.position = new Vector3(x - 0.2f, y, z + 0.2f);
                break;
            case 3:
                occupiedList[0].transform.position = new Vector3(x + 0.3f, y, z - 0.3f);
                occupiedList[1].transform.position = new Vector3(x - 0.3f, y, z + 0.3f);
                occupiedList[2].transform.position = new Vector3(x - 0.3f, y, z - 0.3f);
                break;
            case 4:
                occupiedList[0].transform.position = new Vector3(x + 0.3f, y, z - 0.3f);
                occupiedList[1].transform.position = new Vector3(x - 0.3f, y, z + 0.3f);
                occupiedList[2].transform.position = new Vector3(x - 0.3f, y, z - 0.3f);
                occupiedList[3].transform.position = new Vector3(x + 0.3f, y, z + 0.3f);
                break;
            case 5:
                occupiedList[0].transform.position = new Vector3(x, y, z);
                occupiedList[1].transform.position = new Vector3(x + 0.4f, y, z - 0.4f);
                occupiedList[2].transform.position = new Vector3(x - 0.4f, y, z + 0.4f);
                occupiedList[3].transform.position = new Vector3(x - 0.4f, y, z - 0.4f);
                occupiedList[4].transform.position = new Vector3(x + 0.4f, y, z + 0.4f);
                break;
        }
    }

}
