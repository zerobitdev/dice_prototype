﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPiece : Unit {
    [Header ("Player Stat")]
    public int playerId;
    public Color colorPlayer;
    public SpriteRenderer baseSprite;
    public Renderer mat;

    public int maxEXP;
    public int EXP;
    public int maxAP;
    public int AP;
    public int level = 1;

    // Use this for initialization
    public override void Start () {
        base.Start();
        spriteRef.sprite = ImageManager.inst.PlayerPawnSprite[playerId];
        maxEXP = GameController.inst.player[playerId].maxExpTable[level-1].maxExp;
        //spriteRef.color = colorPlayer;
        baseSprite.color = colorPlayer;
        mat.material.SetColor("_Color", colorPlayer);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GainEXP(int value = 0)
    {
        EXP += value;
        if (EXP >= maxEXP)
        {
            EXP = EXP - maxEXP;
            level += 1;
            maxEXP = GameController.inst.player[playerId].maxExpTable[level-1].maxExp;
            if (level == 3)
            {
                Debug.Log("Max level reach");
                level = 3;
            }
        }
        Debug.Log("|- Player " + playerId + " <color=yellow>gain</color> " + value + " EXP");
        
        UIController.inst.UpdatePlayerUIDynamic(playerId);
        
    }

    public int LoseEXP(int value = 0)
    {
        int validExp = 0;
        if (buffList.Find(item => item.buffItem == BuffDataType.WANTED) != null)
        {
            value = value * 2;
        }
        if (EXP - value < 0)
        {
            validExp = EXP;
            EXP = 0;      
        }
        else
        {
            EXP -= value;
            validExp = value;
        }
        UIController.inst.UpdatePlayerUIDynamic(playerId);
        Debug.Log("|- Player " + playerId + " <color=red>lost</color> " + value + " EXP");
        return validExp;
    }

    public void GainAP(int value = 0)
    {
        AP += value;
        if (AP > maxAP)
        {
            AP = maxAP;
        }
        UIController.inst.UpdatePlayerUIDynamic(playerId);
        Debug.Log("|- Player " + playerId + " <color=blue>gain</color> " + value + " AP");
    }


}
