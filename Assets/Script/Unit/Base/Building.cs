﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : Unit {
    enum Type
    {
        StartPoint,Castle,Dungeon,Tavern
    }

	// Update is called once per frame
	void Update () {
		
	}

    public override void OnPass(PlayerPiece unit)
    {
        base.OnPass(unit);
        Debug.Log("|-Passed Building ");
    }

    public override void OnStop(PlayerPiece unit)
    {
        base.OnStop(unit);
        Debug.Log("|-Stoped On Building");
    }
}
