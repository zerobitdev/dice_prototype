﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {
    [System.Serializable]
    public class BuffList
    {
        public BuffDataType buffItem;
        public int turnCount;
        public int value = 0;
    }

    [Header ("Unit Stat")]
    public string Name;
    public Sprite sprite;
    public SpriteRenderer spriteRef;
    public int standing;

    public int maxHP;
    public int HP;
    public int ATK;
    public int Block ;
    public int DEF;

    public int ATKBuff = 0;
    public int ATKDebuff = 0;
    public int ATKTotalBuff = 0;
    public int BlockBuff = 0;
    public int DEFDebuff = 0;
    public bool BarrierBuff;

    public int ATKBuff_turn;
    public int ATKDebuff_turn;
    public int ATKTotalBuff_turn;
    public int Block_turn;
    public int BlockBuff_turn;
    public int DEFBuff_turn;
    public int BarrierBuff_turn;

    public List<BuffList> buffList = new List<BuffList>();

    //ATK = ATK + Strengh(positive num) + Weakness(negetive num)

    // Use this for initialization
    public virtual void Start () {
        spriteRef.sprite = sprite;
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public virtual void OnPass(PlayerPiece unit)
    {
        Debug.Log("On Pass");
    }

    public virtual void OnStop(PlayerPiece unit)
    {
        Debug.Log("On Stop");
    }

    public int OnAttack(Unit attacker,int _atk)
    {
        int dmgMulti = 1;
        if (standing == attacker.standing && attacker.GetComponent<PlayerPiece>() == true)
        {
            // Check Critical Damage
            Debug.Log("Critical Damage");
            dmgMulti = 2;
        }

        if (BarrierBuff)
        {
            Debug.Log("Barrier is on active -- NO DAMAGE INFLICT");
            UIController.inst.ShowDamageOnPawn(gameObject, 0, dmgMulti > 1);
            return 0;
        }

        if (Block + BlockBuff >= _atk + attacker.ATKBuff)
        {
            Block = (Block + BlockBuff) - (_atk * dmgMulti);
            UIController.inst.ShowDamageOnPawn(gameObject, 0, dmgMulti > 1);
            Debug.Log("<b>No Damge to Player</b>");
            return 0;
        }
        else
        {
            int calAtk;
            //calAtk = (_atk * dmgMulti) + attacker.ATKBuff - attacker.ATKDebuff - (Block + BlockBuff * (Block <= 0 ? 0:1));
            calAtk = (_atk * dmgMulti) + attacker.ATKTotalBuff + DEFDebuff - (Block + BlockBuff * (Block <= 0 ? 0:1));
            Block = 0;
            HP = HP - calAtk;
            UIController.inst.ShowDamageOnPawn(this.gameObject, calAtk, dmgMulti > 1);
            if (GetComponent<PlayerPiece>())
            {
                //Animation UI shaking --> 
                UIController.inst.playerUIList[this.GetComponent<PlayerPiece>().playerId].animator.SetTrigger("Hurt");
                if (HP < 7)
                {
                    UIController.inst.playerUIList[this.GetComponent<PlayerPiece>().playerId].animator.SetBool("LowHp", true);
                }
                
                Debug.Log("<b>Damage: </b>" + calAtk + " to Player");
                return calAtk;
            }
            else
            {
                Debug.Log("<b>Damage: </b>" + calAtk + " to Monster");
                if (checkDead())
                {
                    if (GetComponent<Monster>())
                    {
                        attacker.GetComponent<PlayerPiece>().GainEXP(GetComponent<Monster>().expReward);
                    }
                }
                return calAtk;
            }
            
        }
        
    }

    public bool checkDead()
    {
        if (HP <= 0)
        {
            if (GetComponent<PlayerPiece>() != null)
            {
                GameController.inst.player[GetComponent<PlayerPiece>().playerId].isDead = true;
            }
            
            Debug.Log("Unit is dead");
            HP = 0;
            BoardController.inst.cellList[standing].RemoveUnitOnCell(this);
            BoardController.inst.unitList.Remove(this);
            
            Destroy(gameObject);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void GainHP(int _value)
    {
        HP += _value;
        Debug.Log("<b>Heal: </b>" + _value + " HP");
        if (HP > maxHP)
        {
            HP = maxHP;
        }
        if (HP >= 7)
        {
            UIController.inst.playerUIList[this.GetComponent<PlayerPiece>().playerId].animator.SetBool("LowHp", false);
        }
    }

    public void GainBlock(int _value)
    {
        Block += _value;
        BlockBuff_turn = 1;
        Debug.Log("<b>Block Gain: </b>" + _value + " ");
    }

    public void AddAtkBuff(int value,int turnCount)
    {
            ATKBuff += value;
            ATKBuff_turn += turnCount;
    }

    public void AddAtkDebuff(int value, int turnCount)
    {
        ATKDebuff += value;
        ATKDebuff_turn += turnCount;
    }

    public void AddAtkTotalBuff(int value, int turnCount,int modifier)
    {
        value = value * modifier;
        if ((ATKTotalBuff > 0 && value > 0) || ATKTotalBuff < 0 && value < 0)
        {
            ATKTotalBuff = ATKTotalBuff + value;
            ATKTotalBuff_turn += turnCount;
            return;
        }

        if (turnCount == ATKTotalBuff_turn)
        {
            ATKTotalBuff = ATKTotalBuff + value;
            ATKTotalBuff_turn = 0;
        }
        else if(turnCount > ATKTotalBuff_turn)
        {
            ATKTotalBuff = value;
            ATKTotalBuff_turn = turnCount - ATKTotalBuff_turn;
        }
        else if(turnCount < ATKTotalBuff_turn)
        {
            ATKTotalBuff = ATKTotalBuff +  value;
            ATKTotalBuff_turn = ATKTotalBuff_turn - turnCount;
        }
        
    }

    public void AddDefBuff(int value, int turnCount)
    {
        DEFDebuff += value;
        DEFBuff_turn += turnCount;
    }
    public void AddBlockBuff(int value, int turnCount)
    {
        BlockBuff += value;
        BlockBuff_turn += turnCount;
    }

    public void AddBarrierBuff(bool value, int turnCount)
    {
        BarrierBuff = true;
        BarrierBuff_turn += turnCount;
    }

    public void AddDebuffStatus(string buffName,BuffDataType buff,int turn,Color _color,int _value)
    {
        //Find this debuff is exist
        if ((buffList.Find(BuffList => BuffList.buffItem == buff) != null))
        {
            BuffList itemTemp = buffList.Find(BuffList => BuffList.buffItem == buff);
            Debug.Log(itemTemp);
            itemTemp.turnCount += turn;
        }
        else
        {
            BuffList item = new BuffList();
            item.buffItem = buff;
            item.turnCount = turn;
            item.value = _value;
            buffList.Add(item);
            UIController.inst.playerUIList[GetComponent<PlayerPiece>().playerId].CreateIconBuff(buff, buffName, _color);
        }
    }

    public void ClearDebuff()
    {
        List<BuffList> temp = buffList.FindAll(item => item.value == -1);
        foreach (BuffList item in temp)
        {
            UIController.inst.playerUIList[GetComponent<PlayerPiece>().playerId].RemoveIconBuff(item.buffItem);
            buffList.Remove(item);
        }
  
    }


    public void checkTurnAllBuff(int order)
    {
        checkTurnAtkBuff(order);
        checkTurnDefBuff(order);
        checkTurnTotalATKBuff(order);
        //checkTurnBlock();
        checkTurnBlockBuff(order);
        checkTurnBarrierBuff(order);
        checkDebuffStatus(order);
    }

    public void checkDebuffStatus(int order)
    {
        List<BuffList> tempList = new List<BuffList>();
        switch (order)
        {
            case 0:
                foreach (BuffList item in buffList)
                {
                    if (item.turnCount > 0)
                    {
                        item.turnCount--;
                    }
                }
                break;
            case 1:
                foreach (BuffList item in buffList)
                {
                    if (item.turnCount <= 0)
                    {
                        tempList.Add(item);

                    }
                }
                for (int i = 0; i < tempList.Count; i++)
                {
                    buffList.Remove(tempList[i]);
                    UIController.inst.playerUIList[this.GetComponent<PlayerPiece>().playerId].RemoveIconBuff(tempList[i].buffItem);
                }
                tempList.Clear();
                break;
        }
        
        

    }

    void checkTurnTotalATKBuff(int order)
    {
        switch (order)
        {
            case 0:
                if (ATKTotalBuff_turn > 0)
                {
                    ATKTotalBuff_turn--;

                }
                break;
            case 1:
                if (ATKTotalBuff_turn <= 0)
                {
                    ATKTotalBuff = 0;
                    ATKTotalBuff_turn = 0;
                }
                break;
        }
    }

    void checkTurnAtkBuff(int order)
    {
        switch (order)
        {
            case 0:
                if (ATKBuff_turn > 0)
                {
                    ATKBuff_turn--;

                }
                break;
            case 1:
                if (ATKBuff_turn <= 0)
                {
                    ATKBuff = 0;
                    ATKBuff_turn = 0;
                }
                break;
        }
    }

    void checkTurnDefBuff(int order)
    {
        switch (order)
        {
            case 0:
                if (DEFBuff_turn > 0)
                {
                    DEFBuff_turn--;
                }
                break;
            case 1:
                if (DEFBuff_turn <= 0)
                {
                    DEFDebuff = 0;
                    DEFBuff_turn = 0;
                }
                break;
        }
    }

    void checkTurnBlock(int order)
    {
        switch (order)
        {
            case 0:
                break;
            case 1:
                if (Block_turn > 0)
                {
                    Block = 0;
                    Block_turn = 0;
                }
                break;
        }
        
    }

    void checkTurnBlockBuff(int order)
    {
        switch (order)
        {
            case 0:
                if (BlockBuff_turn > 0)
                {
                    BlockBuff_turn--;
                }
                break;
            case 1:
                if (BlockBuff_turn <= 0)
                {
                    BlockBuff = 0;
                    BlockBuff_turn = 0;
                }
                break;
        }
    }

    void checkTurnBarrierBuff(int order)
    {
        switch (order)
        {
            case 0:
                if (BarrierBuff_turn > 0)
                {
                    BarrierBuff_turn--;
                }
                break;
            case 1:
                if (BarrierBuff_turn <= 0)
                {
                    BarrierBuff = false;
                    BarrierBuff_turn = 0;
                }
                break;
        }
    }




}
