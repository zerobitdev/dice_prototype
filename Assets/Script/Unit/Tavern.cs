﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tavern : Building {

	// Update is called once per frame
	void Update () {
		
	}

    public override void OnPass(PlayerPiece unit)
    {
        base.OnPass(unit);
        Debug.Log("|-Passed Tavern");
        BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer].GainAP(8);

    }

    public override void OnStop(PlayerPiece unit)
    {
        base.OnStop(unit);
        Debug.Log("|-Stop On Tavern");
        BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer].GainAP(21);
        //End Turn ==> Sent to TurnBoardContoller
        TurnBoardController.inst.EndPhase();
    }
}
