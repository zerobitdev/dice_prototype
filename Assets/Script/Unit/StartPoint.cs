﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPoint : Building {

	// Update is called once per frame
	void Update () {
		
	}

    public override void OnPass(PlayerPiece unit)
    {
        base.OnPass(unit);
        GameController.inst.player[GameController.inst.CurrentPlayer].pawn.GainEXP(10);
        GameController.inst.player[GameController.inst.CurrentPlayer].pawn.GainHP(3);
        UIController.inst.UpdatePlayerUIDynamic(GameController.inst.CurrentPlayer);
    }

    public override void OnStop(PlayerPiece unit)
    {
        base.OnStop(unit);
        Debug.Log("|-Stop On Startpoint");
        GameController.inst.player[GameController.inst.CurrentPlayer].pawn.GainHP(6);

        //End Turn ==> Sent to TurnBoardContoller
        TurnBoardController.inst.EndPhase();
    }
}
