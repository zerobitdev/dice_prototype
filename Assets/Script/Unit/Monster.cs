﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : Unit {

    [Header("Monster Stat")]
    public int expReward;
    public MonsterType typeMonster;


    // Use this for initialization
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update () {

	}

    public override void OnPass(PlayerPiece unit)
    {
        base.OnPass(unit);
        Debug.Log("Monster Passed");
        if (typeMonster == MonsterType.Sentinel)
        {
            if (unit.buffList.Find(BuffList => BuffList.buffItem == BuffDataType.SWIFT) != null)
            {
                Debug.Log("Unit has <b>SWIFT</b> run pass it !");
            }
            else
            {
                unit.OnAttack(this, ATK);
            }  
        }
        
    }

    public override void OnStop(PlayerPiece unit)
    {
        base.OnStop(unit);
        Debug.Log("Monster Stoped");
        if (typeMonster == MonsterType.Sentinel)
        {

        }
        else
        {
            unit.OnAttack(this, ATK);
        }
        
    }

}
