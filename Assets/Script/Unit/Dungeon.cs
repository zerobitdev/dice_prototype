﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dungeon : Building {

	
	// Update is called once per frame
	void Update () {
		
	}

    public override void OnPass(PlayerPiece unit)
    {
        base.OnPass(unit);

    }

    public override void OnStop(PlayerPiece unit)
    {
        base.OnStop(unit);
        UIController.inst.ShowDiceQuest();
        Debug.Log("|-Stop On Dungeon");
    }
}
