﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuffDataType {
    NONE,
    STRENGTH,
    WEAK,
    DEFENSE,
    BARRIER,
    VULNERABLE,
    SHADOW,
    SWIFT,
    INSIGHT,
    EXHAUSTED,
    POISON,
    CHICKEN,
    WANTED
	
}
