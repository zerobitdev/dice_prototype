﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIRef : MonoBehaviour {
    public int playerId;
    public Image playerBg;
    public Image avatar;
    public Text textLevel;
    public Text textName;
    public Slider HpSlider;
    public Slider ApSlider;
    public Slider EXPSlider;
    public Text HpText;
    public Text ApText;
    public Text ExpText;
    public Text BlockText;
    public Image BlockImage;
    public GameObject ArrowUI;
    public Animator animator;
    public BuffMainPrefab[] buffMainRef;
    public RectTransform SmallBuffGroupRef;

    public List<GameObject> buffIconList = new List<GameObject>();
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CreateIconBuff(BuffDataType _id,string _name,Color color)
    {
        GameObject obj = Instantiate(UIController.inst.iconPrefab, SmallBuffGroupRef);
        buffIconList.Add(obj);
        obj.GetComponent<SmallBuff>().assignID(_id);
        obj.GetComponent<SmallBuff>().changeSprite(_name);
        obj.GetComponent<SmallBuff>().changeColor(color);
        
    }

    public void RemoveIconBuff(BuffDataType _id)
    {
        GameObject temp = buffIconList.Find(item => item.GetComponent<SmallBuff>().id == _id);
        buffIconList.Remove(temp);
        Destroy(temp);
    }

}
