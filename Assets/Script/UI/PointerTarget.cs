﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointerTarget : MonoBehaviour {

    public Button ButtonRef;
    public Unit unitRef;

    public void Start()
    {
        
    }

    public void ShowTargetNormal()
    {
        //check not to self target
        if (unitRef.GetComponent<PlayerPiece>() == true && GameController.inst.CurrentPlayer == unitRef.GetComponent<PlayerPiece>().playerId )
        {
            Destroy(gameObject);
        }
        //check SHADOW buff
        else if (unitRef.buffList.Find(item => item.buffItem == BuffDataType.SHADOW) != null)
        {
            Debug.Log("This unit has SHADOW");
            Destroy(gameObject);
        }
        else
        {
            AddInteractionStart();
        }
        
    }

    void AddInteractionStart()
    {
        ButtonRef.onClick.AddListener(() =>
        {
            GameController.inst.currentTarget.Add(unitRef);
            UIController.inst.ClosePopupSelectTarget();
            CardController.inst.CardActivate();

        });
    }

}
