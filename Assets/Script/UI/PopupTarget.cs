﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupTarget : MonoBehaviour {
    public Button cancel;
	// Use this for initialization
	void Start () {
        cancel.onClick.AddListener(() =>
            {
                CardController.inst.CardCancel();
                Debug.Log("Cancel");
            });
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
