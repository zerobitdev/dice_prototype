﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Redraw : MonoBehaviour {
    public Button button;
    public Text text;
	// Use this for initialization
	void Start () {
        button.onClick.AddListener(() =>
        {
            if (CardController.inst.cardIsPlaying)
            {
                return;
            }
            CardController.inst.Redraw(GameController.inst.CurrentPlayer);
        });
	}
	
}
