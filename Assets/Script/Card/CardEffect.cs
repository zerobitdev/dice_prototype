﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CardEffect {

    public enum EffectType
    {
        DamageTarget,
        DamageAllPlayer,
        RecoverHpSelf,
        RecoverApSelf,
        GainBlockSelf,
        BuffAtkSelf,
        BuffBlockSelf,
        BuffDefSelf,
        BarrierSelf,
        DebuffAtkTarget,
        DebuffBlockTarget,
        DebuffDefTarget,
        ChickTarget,
        WantedTarget,
        PoisonTarget,
        ExhaustedTarget,
        SwiftSelf,
        InsightSelf,
        AddCardToDeck,
        ShadowSelf,
        ClearDebuffSelf,
        DebuffDefSelf
    }

    public EffectType effectType;
    public BuffDataType buffType;
    [Tooltip ("Damage, Value or id of Card(or buff) to add")]
    public int Value1;
    [Tooltip ("Duration or How many card do you want it to add.")]
    public int Value2;
    
}
