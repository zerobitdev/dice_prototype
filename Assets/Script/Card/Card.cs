﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Card : MonoBehaviour {

    public int id;
    public int level;
    public string head;
    public string description;
    public Sprite sprite;

    [Header("Card Ref")]
    public Image cardImageRef;
    public Button buttonCardRef;
    public Text cardLevelRef;
    public Text cardHeadRef;
    public Text cardDesRef;

    // Use this for initialization
    void Start () {
        UpdateCardInfo();
        //buttonCardRef.onClick.AddListener(() => CardController.inst.UseCard(GameController.inst.CurrentPlayer,this));

	}
	
	// Update is called once per frame
	void Update () {

	}

    public void UpdateCardInfo()
    {
        cardImageRef.sprite = sprite;
        cardLevelRef.text = level.ToString();
        cardHeadRef.text = head;
        //cardDesRef.text = description + CardDataController.inst.carddata[id].effect[0].Value1;
        cardDesRef.text = description;
    }

}
