﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CardData{

    public enum Type
    {
        Target,
        NonTarget,
        AllPlayerTarget
    }
    public string cardName;
    public int id;
    public int level;
    public int cost;

    [Header("Visual")]
    [TextArea(3,5)]
    public string description;
    public Sprite sprite;

    [Header ("Effect Type")]
    public Type type;
    public CardEffect[] effect;




}
