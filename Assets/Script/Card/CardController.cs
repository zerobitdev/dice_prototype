﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class CardController : MonoBehaviour {
    public static CardController inst;
    public GameObject cardUIGroup;
    public GameObject cardDetailUIGroup;
    public Text DeckCount;
    public Text OutDeckCount;
    public GameObject cardPrefab;
    public GameObject cardDetailPrefab;
    public CardData castCard;

    GameObject targetObj;
    bool isMouseDrag = false;
    Vector3 screenPosition;
    Vector3 offset;
    Vector3 Origin;
    //Card Detail
    public float cardOffset = 80;
    bool isShow;
    GameObject cardObjDetail;
    Card targetCard;

    //Targeting
    public bool cardIsPlaying;

    List<Player> player = new List<Player>();
    List<GameObject> CardObjList = new List<GameObject>();

    void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        player = GameController.inst.player;
	}
	
	// Update is called once per frame
	void Update () {
        DragDropCard();
	}

    public void CreateDeckForPlayer()
    {
        foreach (Player item in player)
        {
            item.CreateDeckFromOriginDeck();
        }
    }

    public void RemoveCardFromHand(int playerid)
    {
        while(player[playerid].HandDeck.Count != 0)
        {
            int item = player[playerid].HandDeck[0];
            player[playerid].OutDeck.Add(item);
            player[playerid].HandDeck.Remove(item);
        }
        ClearCardFromHand(playerid);
    }
    public void Redraw(int playerid)
    {
        int cost;
        //cost = 10;
        cost = player[playerid].GetAPCost();
        if (cost < 0) cost = 0;

        if (player[playerid].pawn.AP < cost)
        {
            Debug.LogWarning("<b>Not Enough AP</b>");
            UIController.inst.ShowPopupHint("Not Enough AP");
            return;
        }
        else
        {
            RemoveCardFromHand(playerid);
            player[playerid].pawn.AP -= cost;
        }
        DrawCard(playerid, 4);
        UIController.inst.UpdatePlayerUIDynamic(playerid);
        UIController.inst.UpdateRedrawButton(playerid);
    }

    public void DrawCard(int playerid ,int count = 1)
    {
        //Check Card in Deck is Enough
        CheckDeck(playerid);

        //Check if hand is empty
        if (player[playerid].HandDeck.Count == 0)
        {
            //Draw card to full hand
            for (int i = 0; i < count; i++)
            {
                int tempCard = player[playerid].Deck[0];
                player[playerid].HandDeck.Add(tempCard);
                player[playerid].Deck.RemoveAt(0);
                UpdateDeckCount(playerid);
            }
        }
        ShowCardOnHand(playerid);
    }

    void CheckDeck(int playerid)
    {
        if (player[playerid].Deck.Count < 4)
        {
            while (player[playerid].OutDeck.Count != 0) {
                int ran = Random.Range(0, player[playerid].Deck.Count-1);
                int cardNum = player[playerid].OutDeck[0];
                player[playerid].OutDeck.Remove(cardNum);
                player[playerid].Deck.Insert(ran, cardNum);
            }
        }
    }

    public void MoveDiscardToPile(int playerid)
    {
        while (player[playerid].OutDeck.Count != 0)
        {
            int ran = Random.Range(0, player[playerid].Deck.Count - 1);
            int cardNum = player[playerid].OutDeck[0];
            player[playerid].OutDeck.Remove(cardNum);
            player[playerid].Deck.Insert(ran, cardNum);
        }
    }

    public void ShowCardOnHand(int playerid)
    {
        foreach (int item in player[playerid].HandDeck)
        {
            GameObject obj = Instantiate(cardPrefab, cardUIGroup.transform);
            CardDataController.inst.AssignCard(obj.GetComponent<Card>(), item);
            obj.GetComponent<Card>().UpdateCardInfo();
            CardObjList.Add(obj);
        }
    }
  

    public void UpdateDeckCount(int playerid)
    {
        DeckCount.text = player[playerid].Deck.Count.ToString();
        OutDeckCount.text = player[playerid].OutDeck.Count.ToString();

    }

    void ClearCardObjList()
    {
        CardObjList.Clear();
    }

    void StartDragCard()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo;
            targetObj = ReturnClickedObject(out hitInfo);

            if (targetObj != null)
            {
                //Debug.Log(targetObj.name);
                Origin = targetObj.transform.position;
                targetCard = targetObj.GetComponent<Card>();
                isMouseDrag = true;
                //Debug.Log("target position :" + target.transform.position);
                //Convert world position to screen position.
                screenPosition = Camera.main.WorldToScreenPoint(targetObj.transform.position);
                offset = targetObj.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPosition.z));

            }
        }
    }

    void ShowDetailCard()
    {
        // show detail card 1 time
        if (isMouseDrag && !isShow && Input.GetMouseButton(0) && targetObj.transform.position.y < 2.0f)
        {
            isShow = true;
            targetCard.buttonCardRef.gameObject.SetActive(false);
            cardObjDetail = Instantiate(cardDetailPrefab, cardDetailUIGroup.transform);
            cardObjDetail.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y + cardOffset, screenPosition.z));
            CardDataController.inst.AssignCard(cardObjDetail.GetComponent<Card>(), targetCard.id);
        }
        // show detail card movement
        if (cardObjDetail != null && targetObj.transform.position.y < 2.0f)
        {
            cardObjDetail.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y + cardOffset, screenPosition.z));

        }
        // close detail card
        if (cardObjDetail != null && targetObj.transform.position.y > 2.0f)
        {
            targetCard.buttonCardRef.gameObject.SetActive(true);
            CloseCardDetail();
        }
    }

    void CloseCardDetail()
    {
        isShow = false;
        Destroy(cardObjDetail);
    }

    void AttempUseCard()
    {
        if (targetObj == null) return;
        // Attemp to Use Card
        if (Input.GetMouseButtonUp(0) && targetObj.GetComponent<Card>() != null)
        {
            isMouseDrag = false;
            isShow = false;
            if (cardObjDetail != null)
            {
                Destroy(cardObjDetail);
            }
            CheckUseCardCondition();
            if (targetObj != null)
            {
                //Debug.Log(target.transform.position.y);
                if (targetObj.transform.position.y > 2.7f && !cardIsPlaying)
                {
                    cardIsPlaying = true;
                    StartCoroutine(ResetToOriginalPosition());
                    targetCard.buttonCardRef.gameObject.SetActive(false);
                    CardStart(GameController.inst.CurrentPlayer, targetCard);
                }
                else
                {
                    //Reset
                    targetCard.buttonCardRef.gameObject.SetActive(true);
                    StartCoroutine(ResetToOriginalPosition());
                }

            }

        }
    }

    void CheckUseCardCondition()
    {
        if (player[GameController.inst.CurrentPlayer].actionCount == 0)
        {
            Debug.LogWarning("<color=red><b>No more action count</b></color>");
            StartCoroutine(ResetToOriginalPosition());
            UIController.inst.ShowPopupHint("No more action count");
            return;
        }
        if (targetCard.level > BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer].level)
        {
            Debug.LogWarning("<color=red><b>Not enough level to use this card</b></color>");
            StartCoroutine(ResetToOriginalPosition());
            UIController.inst.ShowPopupHint("Not enough level to use this card");
            return;
        }
        if (BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer].buffList.Find(item => item.buffItem == BuffDataType.CHICKEN) != null)
        {
            Debug.LogWarning("<color=red><b>Player is CHICKEN</b></color>");
            StartCoroutine(ResetToOriginalPosition());
            UIController.inst.ShowPopupHint("Player is CHICKEN");
            return;
        }
    }

    void FollowMouseCard()
    {
        if (isMouseDrag)
        {
            //track mouse position.
            Vector3 currentScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPosition.z);

            //convert screen position to world position with offset changes.
            Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + offset;

            //It will update target gameobject's current postion.
            targetObj.transform.position = currentPosition;
        }
    }

    void DragDropCard()
    {
        if (cardIsPlaying) return;
        StartDragCard();

        if (targetObj == null)
        {
            if (cardObjDetail != null)
            {
                CloseCardDetail();
            }   
            return;
        }
        FollowMouseCard();
        ShowDetailCard();
        AttempUseCard();

    }

    public void CardStart(int playerid, Card card)
    {
        //card.buttonCardRef.gameObject.SetActive(false);
        switch (CardDataController.inst.carddata[targetCard.id].type)
        {
            case CardData.Type.Target:
                //Show Pointer - Select Target
                targetObj = null;
                UIController.inst.ShowPopupSelectTarget();
                break;
            case CardData.Type.NonTarget:
                // Self Target
                GameController.inst.currentTarget.Add(BoardController.inst.playerPieceList[playerid]);
                CardActivate();
                break;
            case CardData.Type.AllPlayerTarget:
                // All Target
                foreach (Unit item in BoardController.inst.playerPieceList)
                {
                    if (item.GetComponent<PlayerPiece>().playerId == GameController.inst.CurrentPlayer)
                    {
                        //Don't target self
                        continue;
                    }
                    GameController.inst.currentTarget.Add(item);
                }
                
                CardActivate();
                break;
            default:
                break;
        }

    }

    public void CardActivate()
    {
        //Active Effect
        //CardDataController.inst.carddata[targetCard.id].effect[0].ActiveEffect(GameController.inst.currentTarget);
        castCard = CardDataController.inst.carddata[targetCard.id];
        foreach (Unit item in GameController.inst.currentTarget)
        {
            for (int i = 0; i < castCard.effect.Length; i++)
            {
                EffectController.inst.ActiveEffect(
                castCard.effect[i].effectType,
                BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer],
                item,
                castCard.effect[i].Value1,
                castCard.effect[i].Value2
                );
            }
        }
        GameController.inst.currentTarget.Clear();

        CardEnd(GameController.inst.CurrentPlayer, targetCard);

        //Update UI
        UpdateDeckCount(GameController.inst.CurrentPlayer);
        foreach (Player item in GameController.inst.player)
        {
            UIController.inst.UpdatePlayerUIDynamic(item.id);
            
        }
    }

    public void CardEnd(int playerid, Card card)
    {
        //Add and Remove from list
        int i = CardObjList.IndexOf(card.gameObject);
        player[playerid].RemoveCardFromHand(i);
        CardObjList.RemoveAt(i);
        Destroy(card.gameObject);
        cardIsPlaying = false;
        player[GameController.inst.CurrentPlayer].actionCount -= 1;
        Debug.Log("card:" + GameController.inst.player[playerid].HandDeck.Count);
        UIController.inst.UpdatePlayerUIDynamic(playerid);
        UIController.inst.UpdateRedrawButton(playerid);
        if (GameController.inst.checkWinner())
        {
            GameController.inst.TimerStop();
            return;
        }

        // Check draw if hand out of card
        //DrawCard(GameController.inst.CurrentPlayer, 4);
    }

    public void CardCancel()
    {
        if (cardIsPlaying)
        {
            UIController.inst.ClosePopupSelectTarget();
            cardIsPlaying = false;
            targetCard.buttonCardRef.gameObject.SetActive(true);
        }
        
    }

    public void ClearCardFromHand(int playerid)
    {
        foreach (GameObject item in CardObjList)
        {
            Destroy(item);
        }
        ClearCardObjList();
    }

    GameObject ReturnClickedObject(out RaycastHit hit)
    {
        GameObject target = null;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray.origin, ray.direction * 10, out hit))
        {
            target = hit.collider.gameObject;
        }
        if (target.tag != "Card")
        {
            return null;
        }
        return target;

    }

    IEnumerator ResetToOriginalPosition()
    {
        targetObj.transform.position = Origin;
        yield return 0;

    }

}
