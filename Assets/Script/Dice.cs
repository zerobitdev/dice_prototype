﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dice : MonoBehaviour {
    public Button diceButton;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// Use to change the graphic static of Dice Button
    /// </summary>
    /// <param name="status"></param>
    public void SetDiceStatus(bool status)
    {
        diceButton.interactable = status;
    }
}
