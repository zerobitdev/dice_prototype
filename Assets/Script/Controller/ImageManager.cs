﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ImageManager : MonoBehaviour {
    public static ImageManager inst;
    [Header("Player Sprite")]
    public List<Sprite> PlayerPawnSprite;

    [Header("Monster Sprite")]
    public List<Sprite> MonsterPawnSprite;

    [Header("Building Sprite")]
    public List<Sprite> BuildingSprite;

    [Header("Card Sprite")]
    public List<Sprite> CardSprite;

    [Header("Icon Sprite")]
    public List<Sprite> IconSprite;

    [Header("Dice Sprite")]
    public List<Sprite> DiceSprite;

    private void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
