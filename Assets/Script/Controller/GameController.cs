﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
    public static GameController inst;
    public List<Player> player;
    public int CurrentPlayer;
    bool isRolled = false;
    bool DevMode = false;
    public int DevRollTestValue = 7;
    public List<Unit> currentTarget = new List<Unit>();

    public float maxTimer = 30;
    float timer;
    bool isTimer = false;
   
    void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    // Use this for initialization
    void Start () {
        // -- SETUP PHASE -----------------------------------
        // Start At player 0
        CurrentPlayer = 0;
        timer = maxTimer;

        //Board Setup
        BoardController.inst.GenerateTile();
        BoardController.inst.CreateUnit();
        BoardController.inst.CreatePlayer();

        //Deck Setup
        CardController.inst.CreateDeckForPlayer();

        //Update UI starting game
        for (int i = 0; i < player.Count; i++)
        {
            UIController.inst.UpdatePlayerUIStatic(i);
            UIController.inst.UpdatePlayerUIDynamic(i);
            
        }
        // -- END SETUP PHASE -------------------------------

        //Start Game Send Controller to TurnBoard
        TurnBoardController.inst.PreStartPhase();
    }
	
	// Update is called once per frame
	void Update () {
        // Dev Test
        SwtichDevMode();
        if (DevMode)
        {
            Dev_RollDice();
        }

        TimerControl();

        
	}

    void TimerControl()
    {
        //Check Time up
        if (timer <= 0)
        {
            timer = 0;
            Debug.LogWarning("<b>Time's Up</b>");
            CardController.inst.CardCancel();
            RollDice();
            TimerStop();
        }

        // Time update
        if (isTimer)
        {
            timer -= Time.deltaTime;
            UIController.inst.UpdateTimerUI(timer/maxTimer);
        }
        else
        {
            // Nothing
        }
    }

    public void TimerStart()
    {
        isTimer = true;
    }

    public void TimerStop()
    {
        isTimer = false;
        timer = maxTimer;
    }

    public void RollDice(int value = 0)
    {
        if (isRolled)
        {
            return;
        }

        TimerStop();
        isRolled = true;
        TurnBoardController.inst.DicePhase(value);
        UIController.inst.dice.SetDiceStatus(false);
        UIController.inst.UpdateTimerUI(0);

    }

    public void ResetRoll()
    {
        isRolled = false;
        UIController.inst.dice.SetDiceStatus(true);
        Debug.Log("-Dice Ready-");
    }

    void SwtichDevMode()
    {
        if (Input.GetKeyDown(KeyCode.Backslash))
        {
            DevMode = !DevMode;
            Debug.LogWarning("DevMode: " + DevMode);
        }
    }

    void Dev_RollDice()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            int valueDice = 1;
            RollDice(valueDice);
            UIController.inst.dice.SetDiceStatus(false);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            int valueDice = 2;
            RollDice(valueDice);
            UIController.inst.dice.SetDiceStatus(false);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            int valueDice = 3;
            RollDice(valueDice);
            UIController.inst.dice.SetDiceStatus(false);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            int valueDice = 4;
            RollDice(valueDice);
            UIController.inst.dice.SetDiceStatus(false);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            int valueDice = 5;
            RollDice(valueDice);
            UIController.inst.dice.SetDiceStatus(false);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            int valueDice = 6;
            RollDice(valueDice);
            UIController.inst.dice.SetDiceStatus(false);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            int valueDice = DevRollTestValue;
            RollDice(valueDice);
            UIController.inst.dice.SetDiceStatus(false);
        }
        else 
        {
            // nothing
        }

        
    }

    public bool checkWinner()
    {
        int deadPlayerCount = 0;
        int winnerId = 0;
        foreach (Player item in player)
        {
            if (item.isDead)
            {
                deadPlayerCount++;
            }
            else
            {
                winnerId = item.id;
            }
        }
        if (deadPlayerCount == 3)
        {
            Debug.Log("<b>Game is Over, Player " + winnerId + " Win !</b>");
            UIController.inst.ShowPopupEndGame(player[winnerId].playerName);
            return true;
        }
        return false;
        
    }

    public void LoadGameplay(string _scene)
    {
        SceneManager.LoadScene(_scene);
    }

}
