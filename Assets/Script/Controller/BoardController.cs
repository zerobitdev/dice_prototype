﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BoardController : MonoBehaviour {

    [System.Serializable]
    public class UnitObject
    {
        public GameObject obj;
        public int position;
    }

    public static BoardController inst;
    public Vector3[] cellPosition;
    public GameObject cell;
    public GameObject tileGroupRef;
    public GameObject pieceGroupRef;
    public GameObject playerPrefab;
    public Color[] playerColorList;

    [Header ("List Ref")]
    public List<CellProperty> cellList = new List<CellProperty>();
    public List<PlayerPiece> playerPieceList = new List<PlayerPiece>();
    public List<Unit> unitList = new List<Unit>();

    [Header("Unit Prefab")]
    public UnitObject[] unitPrefab;

    [Header("Monster Prefab")]
    public GameObject[] monsterPrefab;

    [Header("Starting Config")]
    public int StartPosition = 0;

    [Header("Temp List For Moving")]
    public List<CellProperty> tempCellList = new List<CellProperty>();
    

    private void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        
 
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GenerateTile()
    {
        for (int i=0;i<cellPosition.Length;i++)
        {
            GameObject obj = Instantiate(cell, cellPosition[i], Quaternion.identity, tileGroupRef.transform);
            obj.GetComponent<CellProperty>().cellId = i;
            cellList.Add(obj.GetComponent<CellProperty>());
        }
    }

    public void CreatePlayer()
    {
        // Create Multi player for loop 4 times
        // Collection Players
        for (int i=0;i<4;i++)
        {
            //Spawn Player & assign ref
            GameObject obj = Instantiate(playerPrefab, cellList[StartPosition].GetFloorPosition(), Quaternion.identity, pieceGroupRef.transform);
            PlayerPiece playerTemp = obj.GetComponent<PlayerPiece>();
            AssignStatToPlayerPiece(playerTemp, i);
            GameController.inst.player[i].pawn = playerTemp;

            //Add to List
            playerPieceList.Add(obj.GetComponent<PlayerPiece>());
            unitList.Add(obj.GetComponent<PlayerPiece>());
            cellList[StartPosition].AddUnitOnCell(obj.GetComponent<PlayerPiece>());

        }
    }

    public void AssignStatToPlayerPiece(PlayerPiece pawn,int playerid)
    {
        pawn.standing = StartPosition;
        pawn.playerId = playerid;
        pawn.colorPlayer = playerColorList[playerid];
        Player playerTemp = GameController.inst.player[playerid];

        pawn.maxHP = playerTemp.maxHP;
        pawn.maxAP = playerTemp.maxAP;
        pawn.maxEXP = playerTemp.maxEXP;
        pawn.HP = playerTemp.maxHP;
        pawn.AP = playerTemp.maxAP;
        pawn.EXP = 0;
        pawn.level = 1;
    }

    public void ResetBlock(int playerid)
    {
        playerPieceList[playerid].Block = 0;
    }

    public void CountTurnBuffAtStart(int playerid)
    {
        playerPieceList[playerid].checkTurnAllBuff(0);
    }

    public void CountTurnBuffAtEnd(int playerid)
    {
        playerPieceList[playerid].checkTurnAllBuff(1);
    }

    public void CreateUnit()
    {
        for (int i=0;i<unitPrefab.Length;i++)
        {
            GameObject obj = Instantiate(unitPrefab[i].obj, cellList[unitPrefab[i].position].GetFloorPosition(), Quaternion.identity, pieceGroupRef.transform);
            obj.GetComponent<Unit>().standing = unitPrefab[i].position;
            //Add to list
            unitList.Add(obj.GetComponent<Unit>());
            cellList[unitPrefab[i].position].AddUnitOnCell(obj.GetComponent<Unit>());
        }
    }

    public void MovePiece(int playerid,int stepCount)
    {
        PlayerPiece player = playerPieceList[playerid];
        StartCoroutine(MovingPieceFromTile(player, stepCount));
    }

    IEnumerator MovingPieceFromTile(PlayerPiece _player,int step)
    {
        int stepCount = 0;
        for (int i = 0;i<step;i++)
        {
            stepCount++;
            //Remove player from list
            cellList[_player.standing].RemoveUnitOnCell(_player);

            //Move
            _player.transform.position = cellList.Find(o => o.cellId == cellList[_player.standing].nextCell).GetFloorPosition();
            //Update Standing
            _player.standing = cellList[_player.standing].nextCell;

            //Add player to list
            cellList[_player.standing].AddUnitOnCell(_player);

            //Debug.Log("Step: " + i);

            //check tile on pass
            List<Unit> unitTempPass = cellList[_player.standing].occupiedList;
            Debug.Log("Unit in tile " + unitTempPass.Count);

            //Find other building
            Unit tempBuildingPass = unitTempPass.Find(o => o.GetComponent<Building>());

            //If found, Activate OnPass Function
            if (tempBuildingPass != null) tempBuildingPass.OnPass(_player);

            //Find other Monster
            Unit tempMonsterPass = unitTempPass.Find(o => o.GetComponent<Monster>());

            //If found, Activate OnPass Function
            if (tempMonsterPass != null) tempMonsterPass.OnPass(_player);

            if (_player.HP <= 0)
            {
                Debug.Log("Player is dead");
                TurnBoardController.inst.EndPhase();
                break;
            }
            UIController.inst.UpdatePlayerUIDynamic(_player.playerId);
            yield return new WaitForSeconds(0.3f);
        }

        if (stepCount == step)
        {
            Debug.Log("Moving is Done");

            //check tile on stop
            List<Unit> unitTemp = cellList[_player.standing].occupiedList;
            Debug.Log("Unit in tile " + unitTemp.Count);

            //Find other building

            if (unitTemp.Find(o => o.GetComponent<Building>()))
            {
                Unit tempBuilding = unitTemp.Find(o => o.GetComponent<Building>());
                //If found, Activate OnStop Function
                if (tempBuilding != null)
                {
                    tempBuilding.OnStop(_player);
                    //Show Quest or do something and EndPhase() in Building Subclass
                }
            }
            else if (unitTemp.Find(o => o.GetComponent<Monster>()))
            {
                Unit tempMonster = unitTemp.Find(o => o.GetComponent<Monster>());
                //If found, Activate OnStop Function
                if (tempMonster != null)
                {
                    tempMonster.OnStop(_player);
                    //End Turn ==> Sent to TurnBoardContoller
                    TurnBoardController.inst.EndPhase();
                }
            }
            else
            {
                // Found Nothing
                Debug.Log("Found Nothing");
                //End Turn ==> Sent to TurnBoardContoller
                TurnBoardController.inst.EndPhase();
            }

            UIController.inst.UpdatePlayerUIDynamic(_player.playerId);
 
        }
        
    }

    public int GetNearestMonster(int playerId)
    {
        int result = 0;
        Unit mon = unitList.Find(Monster => Monster.standing > playerPieceList[playerId].standing);
        result = mon.standing - playerPieceList[playerId].standing;
        return result-1;
    }

    public void SpawnMonsterOnEmptyTile(bool random,int cell = 1)
    {
        int ran = 0;
        //Find Empty Cell
        List<CellProperty> emptycell = cellList.FindAll(o => o.occupiedList.Count == 0);
        if (emptycell.Count == 0)
        {
            Debug.LogWarning("<b>No Empty cell for monster to spawn</b>");
            return;
        }

        if (random)
        {
            //Random position
            ran = Random.Range(0, emptycell.Count - 1);
            //Spawn Monster
            GameObject obj = Instantiate(MonsterManager.inst.SpawnOnTurn(TurnBoardController.inst.countTurn), emptycell[ran].GetFloorPosition(), Quaternion.identity, pieceGroupRef.transform);
            obj.GetComponent<Unit>().standing = emptycell[ran].cellId;
            unitList.Add(obj.GetComponent<Unit>());

            //Add to occupiedList on Cell
            cellList[emptycell[ran].cellId].AddUnitOnCell(obj.GetComponent<Unit>());
        }
        else
        {
            //Spawn Monster
            GameObject obj = Instantiate(MonsterManager.inst.SpawnOnTurn(TurnBoardController.inst.countTurn), cellList[cell].GetFloorPosition(), Quaternion.identity, pieceGroupRef.transform);
            obj.GetComponent<Unit>().standing = cellList[cell].cellId;
            unitList.Add(obj.GetComponent<Unit>());

            //Add to occupiedList on Cell
            cellList[cellList[cell].cellId].AddUnitOnCell(obj.GetComponent<Unit>());
        }

    }

}
