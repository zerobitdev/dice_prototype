﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnBoardController : MonoBehaviour {

    enum Turn
    {
        Start, Standby, Dice, Move, End
    }

    public static TurnBoardController inst;
    public int countTurn = 0;
    public int SpawnCount = 2;
    Turn currentTurn;
    int DiceNumber;
    int minDice = 1;
    int maxDice = 6;
    int maxActionCount = 1;


    void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {

	}

	// Update is called once per frame
	void Update () {
    }

    public void PreStartPhase()
    {
        //Draw At Start (for 1st turn)
        foreach (Player item in GameController.inst.player)
        {
            CardController.inst.DrawCard(item.id, 4);
            CardController.inst.UpdateDeckCount(item.id);
            CardController.inst.ClearCardFromHand(item.id);
        }
        for (int i=0;i<4;i++)
        {
            BoardController.inst.SpawnMonsterOnEmptyTile(false,Random.Range(1,4)+(i*4));
        }

        StartPhase();
    }

    public void StartPhase()
    {
        currentTurn = Turn.Start;
        if (GameController.inst.CurrentPlayer == 0)
        {
            countTurn += 1;
            UIController.inst.UpdateCountTurn(countTurn);

            if (countTurn != 1)
            {
                for (int i = 0; i < SpawnCount; i++)
                {
                    BoardController.inst.SpawnMonsterOnEmptyTile(true);
                    Debug.Log("----------");
                }
            }
            
            
            
        }
        if (GameController.inst.player[GameController.inst.CurrentPlayer].isDead)
        {
            EndPhase();
            return;
        }
        ShowTurn();
        CardController.inst.UpdateDeckCount(GameController.inst.CurrentPlayer);
        CardController.inst.ShowCardOnHand(GameController.inst.CurrentPlayer);
        GameController.inst.ResetRoll();
        GameController.inst.player[GameController.inst.CurrentPlayer].actionCount = maxActionCount;
        BoardController.inst.ResetBlock(GameController.inst.CurrentPlayer);
        BoardController.inst.CountTurnBuffAtStart(GameController.inst.CurrentPlayer);
        UIController.inst.UpdatePlayerUIDynamic(GameController.inst.CurrentPlayer);
        UIController.inst.UpdateRedrawButton(GameController.inst.CurrentPlayer);
        UIController.inst.SetShowArrowTurn(GameController.inst.CurrentPlayer, true);
        UIController.inst.ShowArrow(GameController.inst.CurrentPlayer);

        // Go to StandyPhase
        StandbyPhase();
    }

    public void StandbyPhase()
    {
        currentTurn = Turn.Standby;
        GameController.inst.TimerStart();
        ShowTurn();
  
    }

    public void DicePhase(int value = 0)
    {
        UIController.inst.SetShowArrowTurn(GameController.inst.CurrentPlayer, false);
        currentTurn = Turn.Dice;
        ShowTurn();
        StartCoroutine(RollDice(value));
    }

    public void MovePhase()
    {
        currentTurn = Turn.Move;
        ShowTurn();
        Moving();
        
    }

    IEnumerator RollDice(int value = 0)
    {
        if (value == 0)
        {
            DiceNumber = Random.Range(minDice, maxDice + 1);
        }
        else
        {
            DiceNumber = value;
        }
        
        Debug.Log("Roll: " + DiceNumber);
        UIController.inst.UpdateDiceResult(DiceNumber);
        CardController.inst.ClearCardFromHand(GameController.inst.CurrentPlayer);
        yield return new WaitForSeconds(1.0f);  
        MovePhase();
    }

    void Moving()
    {
        Debug.Log("--- Moving ---");
        BoardController.inst.MovePiece(GameController.inst.CurrentPlayer, DiceNumber);
        // ==> send to BoardController and wait for coroutine to done

    }

    void ShowTurn()
    {
        Debug.Log("<b>Player: </b>" + GameController.inst.CurrentPlayer + "<b> || Turn -- </b>" + currentTurn);
    }

    public void EndPhase()
    {
        currentTurn = Turn.End;

        //Draw Card At End
        CardController.inst.DrawCard(GameController.inst.CurrentPlayer, 4);
        CardController.inst.UpdateDeckCount(GameController.inst.CurrentPlayer);
        CardController.inst.ClearCardFromHand(GameController.inst.CurrentPlayer);
        BoardController.inst.CountTurnBuffAtEnd(GameController.inst.CurrentPlayer);
        GameController.inst.TimerStop();
        UIController.inst.UpdatePlayerUIDynamic(GameController.inst.CurrentPlayer);
        ShowTurn();
        if (GameController.inst.checkWinner())
        {
            return;
        }

        // move to next player if someone is dead
        GameController.inst.CurrentPlayer = GameController.inst.CurrentPlayer == 3 ? 0: GameController.inst.CurrentPlayer + 1; 

        // Next player turn
        StartPhase();
    }
}
