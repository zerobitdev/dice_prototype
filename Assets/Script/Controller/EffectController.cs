﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectController : MonoBehaviour {
    public static EffectController inst;

    private void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
	
    public void ActiveEffect(CardEffect.EffectType effect, Unit caster = null,Unit target = null,int value1 = 0,int value2 = 0)
    {
        switch (effect)
        {
            case CardEffect.EffectType.DamageTarget:
                int validExp = 0;
                Debug.Log("<b>Use Damage Card</b>");
                if (target == null)
                {
                    Debug.Log("No target");
                    return;
                }
                validExp = target.OnAttack(caster, value1);
                if (target.GetComponent<PlayerPiece>() == true)
                {
                    //Animation UI shaking --> 
                    //UIController.inst.playerUIList[target.GetComponent<PlayerPiece>().playerId].animator.SetTrigger("Hurt");
                    validExp = target.GetComponent<PlayerPiece>().LoseEXP(validExp);
                    caster.GetComponent<PlayerPiece>().GainEXP(validExp);
                }
                else
                {
                    // Attack Monster
                }
                break;
            case CardEffect.EffectType.DamageAllPlayer:
                Debug.Log("<b>Use Damage All player card</b>");
                validExp = value1 * 1;
                validExp = target.GetComponent<PlayerPiece>().LoseEXP(validExp);
                caster.GetComponent<PlayerPiece>().GainEXP(validExp);
                target.OnAttack(caster, value1);
                break;
            case CardEffect.EffectType.RecoverHpSelf:
                Debug.Log("<b>Use Recover HP Card</b>");
                caster.GainHP(value1);
                break;
            case CardEffect.EffectType.RecoverApSelf:
                Debug.Log("<b>Use Recover AP Card</b>");
                caster.GetComponent<PlayerPiece>().GainAP(value1);
                break;
            case CardEffect.EffectType.GainBlockSelf:
                Debug.Log("<b>Use Block Card</b>");
                caster.Block += value1;
                break;
            case CardEffect.EffectType.AddCardToDeck:
                Debug.Log("<b>Use Add Item Card</b>");
                //Add Card
                GameController.inst.player[caster.GetComponent<PlayerPiece>().playerId].Deck.Add(value1);
                CardController.inst.UpdateDeckCount(caster.GetComponent<PlayerPiece>().playerId);
                break;
            case CardEffect.EffectType.BuffAtkSelf:
                Debug.Log("Buff Atk Self");
                caster.AddAtkBuff(value1, value2);
                caster.AddAtkTotalBuff(value1, value2, +1);
                break;
            case CardEffect.EffectType.BuffBlockSelf:
                Debug.Log("Buff Block Self");
                caster.AddBlockBuff(value1, value2);
                break;
            case CardEffect.EffectType.BuffDefSelf:
                Debug.Log("Buff Block Self");
                caster.AddDefBuff(value1, value2);
                break;
            case CardEffect.EffectType.DebuffAtkTarget:
                if (target.GetComponent<Monster>()) break;
                Debug.Log("Debuff Atk Target");
                target.AddAtkDebuff(value1, value2);
                target.AddAtkTotalBuff(value1, value2, -1);
                break;
            case CardEffect.EffectType.DebuffBlockTarget:
                if (target.GetComponent<Monster>()) break;
                Debug.Log("Debuff Atk Target");
                target.AddBlockBuff(value1, value2);
                break;
            case CardEffect.EffectType.DebuffDefTarget:
                if (target.GetComponent<Monster>()) break;
                Debug.Log("Debuff Atk Target");
                target.AddDefBuff(value1, value2);
                break;
            case CardEffect.EffectType.DebuffDefSelf:
                Debug.Log("Debuff Atk Target");
                caster.AddDefBuff(value1, value2);
                break;
            case CardEffect.EffectType.BarrierSelf:
                Debug.Log("Buff Barrier");
                caster.AddBarrierBuff(true, value2);
                caster.AddDebuffStatus("icon_barrier", BuffDataType.BARRIER, value2,Color.yellow,1);
                break;
            case CardEffect.EffectType.ChickTarget:
                if (target.GetComponent<Monster>()) break;
                Debug.Log("Chicken");
                target.AddDebuffStatus("icon_chicken",BuffDataType.CHICKEN,value2, Color.red,-1);
                break;
            case CardEffect.EffectType.ShadowSelf:
                Debug.Log("Shadow");
                caster.AddDebuffStatus("icon_shadow",BuffDataType.SHADOW, value2, Color.green,1);
                break;
            case CardEffect.EffectType.SwiftSelf:
                Debug.Log("Swift");
                caster.AddDebuffStatus("icon_swift",BuffDataType.SWIFT, value2, Color.green,1);
                break;
            case CardEffect.EffectType.InsightSelf:
                Debug.Log("Insight");
                caster.AddDebuffStatus("icon_insight",BuffDataType.INSIGHT, value2, Color.green,1);
                GameController.inst.player[caster.GetComponent<PlayerPiece>().playerId].redrawCost = value1;
                break;
            case CardEffect.EffectType.ExhaustedTarget:
                if (target.GetComponent<Monster>()) break;
                Debug.Log("Exhausted");
                target.AddDebuffStatus("icon_exhausted",BuffDataType.EXHAUSTED, value2, Color.red,-1);
                GameController.inst.player[target.GetComponent<PlayerPiece>().playerId].redrawCost = value1;
                break;
            case CardEffect.EffectType.PoisonTarget:
                if (target.GetComponent<Monster>()) break;
                Debug.Log("Poison");
                target.AddDebuffStatus("icon_poison",BuffDataType.POISON, value2, Color.red,-1);
                break;
            case CardEffect.EffectType.WantedTarget:
                if (target.GetComponent<Monster>()) break;
                Debug.Log("Wanted");
                target.AddDebuffStatus("icon_wanted",BuffDataType.WANTED, value2, Color.red,-1);
                break;
            case CardEffect.EffectType.ClearDebuffSelf:
                Debug.Log("Clear Debuff");
                caster.ClearDebuff();
                break;
            default:
                Debug.LogWarning("NO EFFECT Active");
                break;
        }
    }
}
