﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDataController : MonoBehaviour {
    public static CardDataController inst;

    public CardData[] carddata;

    void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AssignCard(Card cardPrefab,int cardId = 0)
    {
        CardData data = carddata[cardId];
        cardPrefab.id = cardId;
        cardPrefab.level = data.level;
        cardPrefab.head = data.cardName;
        cardPrefab.description = data.description;
        cardPrefab.sprite = data.sprite;
    }
}
