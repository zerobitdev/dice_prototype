﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class UIController : MonoBehaviour {
    public static UIController inst;
    public Text countTurnText;
    public Text diceText;
    public Dice dice;
    public List<PlayerUIRef> playerUIList;
    [Header("UI Ref")]
    public TimerUI timerui;
    public Redraw redrawui;
    public RedrawSlider redrawSlider;

    [Header("UIGroup Ref")]
    public GameObject UIPopupGroupRef;
    public GameObject DamgeGroupRef;

    [Header("Popup Prefab")]
    public GameObject popupTarget;
    public GameObject Pointer;
    public GameObject DamagePopup;
    public GameObject popupBox;
    public GameObject popupHint;
    public GameObject popupDiceQuest;
    public GameObject popupSelectQuest;
    public GameObject ArrowPiece;

    [Header("Icon Debuff Prefab")]
    public GameObject iconPrefab;

    GameObject objTemp;
    List<GameObject> pointerList = new List<GameObject>();
    List<GameObject> popupList = new List<GameObject>();

    void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    // Use this for initialization
    void Start () {
        
        dice.diceButton.onClick.AddListener(() => {
            if (CardController.inst.cardIsPlaying)
            {
                return;
            }
            GameController.inst.RollDice();
            dice.SetDiceStatus(false);
        });
    }
	
	// Update is called once per frame
	void Update () {

	}

    public void UpdateTimerUI(float x)
    {
        timerui.timeImage.fillAmount = x;
    }

    public void ShowDamageOnPawn(GameObject target,int damage,bool isCritical)
    {
        GameObject obj = Instantiate(DamagePopup, DamgeGroupRef.transform);
        obj.GetComponent<DamagePop>().unitTarget = target;
        obj.GetComponent<DamagePop>().DamageTextRef.text = damage.ToString();
        if (isCritical)
        {
            obj.GetComponent<DamagePop>().DamageTextRef.color = Color.red;
        }
        Destroy(obj, 1.0f);

    }

    public void ShowPopupSelectTarget()
    {
        objTemp = Instantiate(popupTarget, UIPopupGroupRef.transform);
        CardController.inst.cardIsPlaying = true;
        foreach (Unit item in BoardController.inst.unitList)
        {
            if (item.GetComponent<Building>() == true)
            {
                continue;
            }
            else
            {
                ShowTargetPointer(item);
            }
            
        }
    }

    public void ShowTargetPointer(Unit unit)
    {
        GameObject obj = Instantiate(Pointer, UIPopupGroupRef.transform);
        obj.transform.position = new Vector3(unit.transform.position.x,unit.transform.position.y + 0.3f,unit.transform.position.z);
        obj.GetComponent<PointerTarget>().unitRef = unit;
        obj.GetComponent<PointerTarget>().ShowTargetNormal();
        pointerList.Add(obj);
    }

    public void ClosePopupSelectTarget()
    {
        if (objTemp != null)
        {
            Destroy(objTemp);
            foreach(GameObject item in pointerList)
            {
                Destroy(item);
            }
            pointerList.Clear();
        }
    }

    public void ShowArrow(int playerid)
    {
        GameObject obj = Instantiate(ArrowPiece, UIPopupGroupRef.transform);
        obj.GetComponent<ArrowPiece>().showArrowOnUnit(BoardController.inst.playerPieceList[playerid].gameObject,BoardController.inst.playerColorList[playerid]);
        Destroy(obj, 2.0f);
    }

    public void UpdateCountTurn(int countTurn)
    {
        countTurnText.text = countTurn.ToString();
        
    }

    public void UpdateDiceResult(int num)
    {
        diceText.text = num.ToString();
    }

    public void UpdatePlayerUIStatic(int playerid)
    {
        Player player = GameController.inst.player[playerid];
        PlayerUIRef uiRef = playerUIList.Find(o => o.playerId == playerid);
  
        uiRef.textName.text = player.playerName;
        uiRef.avatar.sprite = player.avatar;
        uiRef.playerBg.color = BoardController.inst.playerColorList[player.id];
        
    }

    public void UpdatePlayerUIDynamic(int playerid)
    {
        Player player = GameController.inst.player[playerid];
        PlayerPiece playerPiece = BoardController.inst.playerPieceList[playerid];
        PlayerUIRef playerUIRef = playerUIList.Find(o => o.playerId == playerid);
        //Level
        playerUIRef.textLevel.text = player.pawn.level.ToString();
        //HP
        playerUIRef.HpSlider.maxValue = player.maxHP;
        playerUIRef.HpSlider.value = playerPiece.HP;
        playerUIRef.HpText.text = "<color=red>"+playerPiece.HP.ToString()+"</color>"+"/"+player.maxHP.ToString();
        //AP
        playerUIRef.ApSlider.maxValue = player.maxAP;
        playerUIRef.ApSlider.value = playerPiece.AP;
        playerUIRef.ApText.text = "<color=#00a1ffff>"+playerPiece.AP.ToString() + "</color>"+"/" + player.maxAP.ToString();
        //EXP
        playerUIRef.EXPSlider.maxValue = playerPiece.maxEXP;
        playerUIRef.EXPSlider.value = playerPiece.EXP;
        if (playerPiece.level == 3)
        {
            playerUIRef.ExpText.text = playerPiece.EXP.ToString() + " EXP";
            //playerUIRef.EXPSlider.value = playerUIRef.EXPSlider.maxValue;
        }
        else
        {
            playerUIRef.ExpText.text = playerPiece.EXP.ToString() + "/" + playerPiece.maxEXP.ToString() + " EXP";
        }
        //Block
        if (playerPiece.Block == 0)
        {
            playerUIRef.BlockImage.gameObject.SetActive(false);
            playerUIRef.BlockText.gameObject.SetActive(false);
        }
        else
        {
            playerUIRef.BlockImage.gameObject.SetActive(true);
            playerUIRef.BlockText.gameObject.SetActive(true);
            playerUIRef.BlockText.text = playerPiece.Block.ToString();
        }

        //Update Buff
        playerUIRef.buffMainRef[0].UpdateBuffText(playerPiece.BlockBuff, playerPiece.BlockBuff_turn);
        playerUIRef.buffMainRef[1].UpdateBuffText(playerPiece.ATKTotalBuff,playerPiece.ATKTotalBuff_turn);
        playerUIRef.buffMainRef[2].UpdateBuffText(playerPiece.DEFDebuff * -1, playerPiece.DEFBuff_turn);
    }

    public void UpdateRedrawButton(int playerid)
    {
        Player player = GameController.inst.player[playerid];
        //Text AP Redraw Update
        if (player.redrawCost > 0)
        {
            redrawui.text.text = "Redraw AP " + "<color=red>" + player.GetAPCost().ToString() + "</color>";
        }
        else if (player.redrawCost < 0)
        {
            redrawui.text.text = "Redraw AP " + "<color=lime>" + player.GetAPCost().ToString() + "</color>";
        }
        else
        {
            redrawui.text.text = "Redraw AP " + "<color=white>" + player.GetAPCost().ToString() + "</color>";
        }
        //redraw slider
        redrawSlider.apText.text = "<color=white>" + player.GetAPCost().ToString() + "</color>";
    }

    public void SetShowArrowTurn(int playerid,bool value)
    {
        playerUIList[playerid].ArrowUI.SetActive(value);
    }

    public void ShowPopupEndGame(string _playerName)
    {
        GameObject obj = Instantiate(popupBox, UIPopupGroupRef.transform);
        obj.GetComponent<PopupBox>().setVictoryText(_playerName);
    }

    public void ShowPopupHint(string hintText)
    {
        GameObject obj = Instantiate(popupHint,UIPopupGroupRef.transform);
        obj.GetComponent<PopupHint>().SetHintText(hintText);
        Destroy(obj, 1.2f);
    }

    public void ShowDiceQuest()
    {
        GameObject obj = Instantiate(popupDiceQuest, UIPopupGroupRef.transform);
        obj.GetComponent<PopupDiceQuest>().SetQuestPopup(QuestDataController.inst.RandomDiceQuestID());
    }

    public void ShowSelectQuest()
    {
        GameObject obj = Instantiate(popupSelectQuest, UIPopupGroupRef.transform);
        obj.GetComponent<PopupSelectQuest>().SetQuestPopup(QuestDataController.inst.RandomDiceQuestID());
    }
}

