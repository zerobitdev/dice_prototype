﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    [System.Serializable]
    public class ExpData
    {
        public int maxExp;
    }

    public int id;
    [Header ("Starting Stat")]
    public int HP;
    public int maxHP;
    public int AP;
    public int maxAP;
    public int EXP;
    public int maxEXP;
    public ExpData[] maxExpTable;
    public int Level;
    public int Block;
    public bool isDead = false;

    [Header("Action Count")]
    public int actionCount = 1;
    public int redrawCost = 0;

    [Header ("Deck")]
    public List<int> OriginDeck = new List<int>();

    [Header("Card")]
    public List<int> Deck = new List<int>();
    public List<int> OutDeck = new List<int>();
    public List<int> HandDeck = new List<int>();

    [Header("Other")]
    public Sprite avatar;
    public string playerName;

    public PlayerPiece pawn;



    // Use this for initialization
    void Start () {
        

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CreateDeckFromOriginDeck()
    {
        foreach (int item in OriginDeck)
        {
            int ran = Random.Range(0, Deck.Count - 1);
            Deck.Insert(ran,item);
        }
    }

    public void RemoveCardFromHand(int cardIndex)
    {
        OutDeck.Add(HandDeck[cardIndex]);
        HandDeck.RemoveAt(cardIndex);
    }

    public int GetAPCost()
    {
        int APcost = 3 * HandDeck.Count + redrawCost;
        return APcost;
    }

}
