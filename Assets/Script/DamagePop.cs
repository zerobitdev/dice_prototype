﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamagePop : MonoBehaviour {

    public Image DamageImageRef;
    public Text DamageTextRef;
    public GameObject unitTarget;
   // public bool trigger;
    public float offsetY = 1.0f; 

	// Use this for initialization
	void Start () {
        Vector3 offset = new Vector3(0, offsetY, 0);
        transform.position = unitTarget.transform.position + offset;
    }
	
	// Update is called once per frame
	void Update () {
        //if (trigger)
        //{
        //    trigger = false;
        //    Vector3 offset = new Vector3(0, offsetY, 0);
        //    transform.position = unitTarget.transform.position + offset;
        //}
	}
}
