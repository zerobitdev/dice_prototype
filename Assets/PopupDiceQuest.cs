﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupDiceQuest : MonoBehaviour {
    public Text headQuest;
    public Image imageQuest;
    public Text desText;
    public Text choiceText1;
    public Text choiceText2;
    public Text choiceText3;
    public Text choiceText4;
    public Text choiceText5;
    public Image[] frame;
    public Button diceButton;
    public Slider timerSlider;
    public float maxTimer = 10;
    float time;
    int choiceCount;
    int questid;
    bool timeStop = false;
    public Image[] dice1, dice2, dice3,dice4,dice5;
    // Use this for initialization
    void Start () {
        timerSlider.maxValue = maxTimer;
        timerSlider.value = timerSlider.maxValue;
        time = maxTimer;
        timeStop = false;
        diceButton.onClick.AddListener(()=>
        {
            time = 0;
            timerSlider.value = time;
        });
    }
	
	// Update is called once per frame
	void Update () {
        Timer();
	}

    void Timer()
    {
        if (time > 0)
        {
            time -= Time.deltaTime;
            timerSlider.value = time;
        }
        else if(time <= 0 && timeStop == false)
        {
            Debug.Log("Timer Stop");
            timeStop = true;
            time = 0;
            int result = RollDiceChoice(choiceCount);
            QuestDataController.inst.DiceQuestEvent(questid, result);
            Destroy(this.gameObject);
        }
    }

    int RollDiceChoice(int _range)
    {
        int ran = Random.Range(0, _range);
        Debug.Log("Choice Select: " + ran);
        return ran;
    }

    public void SetQuestPopup(int id)
    {
        headQuest.text = QuestDataController.inst.diceQuestData[id].questName;
        //imageQuest.sprite = QuestDataController.inst.diceQuestData[id].spriteName;
        desText.text = QuestDataController.inst.diceQuestData[id].description;
        choiceCount = QuestDataController.inst.diceQuestData[id].choiceSize;
        questid = QuestDataController.inst.diceQuestData[id].id;
        if (choiceCount == 3)
        {
            frame[0].gameObject.SetActive(true);
            frame[1].gameObject.SetActive(true);
            frame[2].gameObject.SetActive(true);
            frame[3].gameObject.SetActive(false);
            frame[4].gameObject.SetActive(false);
            choiceText1.text = QuestDataController.inst.diceQuestData[id].choiceText0;
            choiceText2.text = QuestDataController.inst.diceQuestData[id].choiceText1;
            choiceText3.text = QuestDataController.inst.diceQuestData[id].choiceText2;
            dice1[0].sprite = ImageManager.inst.DiceSprite[0];
            dice1[1].sprite = ImageManager.inst.DiceSprite[1];
            dice2[0].sprite = ImageManager.inst.DiceSprite[2];
            dice2[1].sprite = ImageManager.inst.DiceSprite[3];
            dice3[0].sprite = ImageManager.inst.DiceSprite[4];
            dice3[1].sprite = ImageManager.inst.DiceSprite[5];
        }
        else
        {
            frame[0].gameObject.SetActive(false);
            frame[1].gameObject.SetActive(false);
            frame[2].gameObject.SetActive(false);
            frame[3].gameObject.SetActive(true);
            frame[4].gameObject.SetActive(true);
            choiceText1.gameObject.SetActive(false);
            choiceText2.gameObject.SetActive(false);
            choiceText3.gameObject.SetActive(false);
            choiceText4.gameObject.SetActive(true);
            choiceText5.gameObject.SetActive(true);
            choiceText4.text = QuestDataController.inst.diceQuestData[id].choiceText0;
            choiceText5.text = QuestDataController.inst.diceQuestData[id].choiceText1;
            dice4[0].sprite = ImageManager.inst.DiceSprite[0];
            dice4[1].sprite = ImageManager.inst.DiceSprite[1];
            dice4[2].sprite = ImageManager.inst.DiceSprite[2];
            dice5[0].sprite = ImageManager.inst.DiceSprite[3];
            dice5[1].sprite = ImageManager.inst.DiceSprite[4];
            dice5[2].sprite = ImageManager.inst.DiceSprite[5];
        }
    }
}
