﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupBox : MonoBehaviour {
    public Text victoryText;
    public Button nextButton;
	// Use this for initialization
	void Start () {
        nextButton.onClick.AddListener(() =>
        {
            GameController.inst.LoadGameplay("01_main_phototype");
            Debug.Log("--- Load Gameplay ---");
        }
        );
    }

    public void setVictoryText(string _word)
    {
        victoryText.text = _word + " Victory ! ";
    }
	
}
