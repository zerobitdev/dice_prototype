﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuffMainPrefab : MonoBehaviour {
    public Sprite imageSprite;
    public Image BuffImage;
    public Text BuffText;
    public Text TurnText;

    public void Start()
    {
        BuffImage.sprite = imageSprite;
    }

    public void UpdateBuffText(int buffPower,int buffTurn)
    {
        string BuffString = "0";
        if (buffPower < 0)
        {
            BuffString = "<color=red>" + buffPower.ToString() +"</color>";
            gameObject.SetActive(true);
        }
        else if (buffPower > 0)
        {
            BuffString = "<color=lime>+" + buffPower.ToString() + "</color>";
            gameObject.SetActive(true);
        }
        else
        {
            BuffString = "<color=white>" + buffPower.ToString() + "</color>";
            gameObject.SetActive(false);
            
        }
        BuffText.text = BuffString;
        TurnText.text = buffTurn.ToString();
    }
}
