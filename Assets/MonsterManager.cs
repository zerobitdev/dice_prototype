﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterManager : MonoBehaviour {
    public static MonsterManager inst;

    [System.Serializable]
    public class MonsterData
    {
        public string MonsterName;
        public int atk;
        public int hp;
        public int rewardXp;
        public MonsterType monsterType;
        public string spriteName;
    }
    
    [System.Serializable]
    public class MonsterSpawnTurn
    {
        public MonsterRate[] MonsterCount;
    }

    [System.Serializable]
    public class MonsterRate
    {
        public int MonsterID;
        public int SpawnRate;
    }

    [Header ("Monster Data")]
    public List<MonsterData> monsterData = new List<MonsterData>();

    [Header("Monster Spawn Rate")]
    public MonsterSpawnTurn[] spawnData;

    public GameObject MonsterPrefab;

    void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Start()
    {

    }

    public GameObject AssignMonster(int id)
    {
        GameObject obj = MonsterPrefab;
        Monster objTemp = obj.GetComponent<Monster>();
        objTemp.Name = monsterData[id].MonsterName;
        objTemp.ATK = monsterData[id].atk;
        objTemp.HP = monsterData[id].hp;
        objTemp.expReward = monsterData[id].rewardXp;
        objTemp.typeMonster = monsterData[id].monsterType;
        objTemp.sprite = ImageManager.inst.MonsterPawnSprite[ImageManager.inst.MonsterPawnSprite.FindIndex(item => item.name == monsterData[id].spriteName)];
        return obj;
    }

    public GameObject SpawnOnTurn(int turn)
    {
        int ran = Random.Range(0,100);
        
        for (int i=0;i<spawnData.Length;i++)
        {
            if (ran - spawnData[turn].MonsterCount[i].SpawnRate <= 0)
            {
                int id = spawnData[turn].MonsterCount[i].MonsterID;
                GameObject obj = AssignMonster(id);
                return obj;
            }
            else
            {
                ran = ran - spawnData[turn].MonsterCount[i].SpawnRate;
            }
        }
        Debug.LogError("Spawn Error");
        return null;
        
        
    }


}
