﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupSelectQuest : MonoBehaviour {
    public Text headQuest;
    public Image imageQuest;
    public Text desText;
    public Button choice1;
    public Button choice2;
    public Text choiceText1;
    public Text choiceText2;
    public Slider timerSlider;
    public float maxTimer = 10;
    float time;
    int choiceCount = 2;
    int questid;
    bool timeStop = false;
    // Use this for initialization
    void Start () {
        timerSlider.maxValue = maxTimer;
        timerSlider.value = timerSlider.maxValue;
        time = maxTimer;
        timeStop = false;
        choice1.onClick.AddListener(() =>
        {
            timerSlider.value = time;
            choiceCount = 0;
            QuestDataController.inst.SelectQuestEvent(questid, choiceCount);
            Destroy(gameObject);
        });
        choice2.onClick.AddListener(() =>
        {
            timerSlider.value = time;
            choiceCount = 1;
            QuestDataController.inst.SelectQuestEvent(questid, choiceCount);
            Destroy(gameObject);
        });
    }

    void Update()
    {
        Timer();
    }

    void Timer()
    {
        if (time > 0)
        {
            time -= Time.deltaTime;
            timerSlider.value = time;
        }
        else if (time <= 0 && timeStop == false)
        {
            Debug.Log("Timer Stop");
            timeStop = true;
            time = 0;
            int result = RollDiceChoice(choiceCount);
            QuestDataController.inst.SelectQuestEvent(questid, result);
            Destroy(gameObject);
        }
    }

    int RollDiceChoice(int _range)
    {
        int ran = Random.Range(0, _range);
        Debug.Log("Choice Select: " + ran);
        return ran;
    }

    public void SetQuestPopup(int id)
    {
        headQuest.text = QuestDataController.inst.selectQuestData[id].questName;
        //imageQuest.sprite = QuestDataController.inst.diceQuestData[id].spriteName;
        desText.text = QuestDataController.inst.selectQuestData[id].description;
        choiceCount = QuestDataController.inst.selectQuestData[id].choiceSize;
        questid = QuestDataController.inst.selectQuestData[id].id;
        if (choiceCount == 2)
        {
            choiceText1.text = QuestDataController.inst.selectQuestData[id].choiceText0;
            choiceText2.text = QuestDataController.inst.selectQuestData[id].choiceText1;
        }
    }

}
