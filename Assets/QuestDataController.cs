﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestDataController : MonoBehaviour {
    public static QuestDataController inst;

    [System.Serializable]
    public class SelectQuestData
    {
        public string questName;
        public int id;
        [TextArea (2,2)]
        public string description;

        public string spriteName;
        public int choiceSize;
        public string choiceText0;
        public string choiceText1;
    }

    [System.Serializable]
    public class DiceQuestData
    {
        public string questName;
        public int id;
        [TextArea(2, 2)]
        public string description;

        public string spriteName;
        public int choiceSize;
        public string choiceText0;
        public string choiceText1;
        public string choiceText2;
        public int[] dice1, dice2, dice3;
    }

    [Header("Select Quest")]
    public SelectQuestData[] selectQuestData;

    [Header("Dice Quest")]
    public DiceQuestData[] diceQuestData;

    private void Awake()
    {
        if (inst == null)
        {
            inst = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int RandomDiceQuestID()
    {
        int ran = Random.Range(0, diceQuestData.Length);
        return ran;
    }

    public int RandomSelectQuestID()
    {
        int ran = Random.Range(0, selectQuestData.Length);
        return ran;
    }

    public void DiceQuestEvent(int id,int choice)
    {
        Unit tempUnit = new Unit();
        switch (id)
        {
            case 0:
                if(choice == 0)
                {
                    //Quest choice result 0
                    Debug.Log("Quest 0 Event choice 0");
                    BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer].OnAttack(tempUnit, 5);
                    UIController.inst.ShowPopupHint("You take 5 Damage");
                }
                else
                {
                    //Quest choice result 1
                    Debug.Log("Quest 0 Event choice 1");
                    UIController.inst.ShowPopupHint("You Escaped!");
                }
                break;
            case 1:
                if (choice == 0)
                {
                    //Quest choice result 0
                    Debug.Log("Quest 1 Event choice 0");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.AddCardToDeck,BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer],null,22);
                    UIController.inst.ShowPopupHint("You got 1 Elder Potion");
                }
                else if(choice == 1)
                {
                    //Quest choice result 1
                    Debug.Log("Quest 1 Event choice 1");
                    BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer].OnAttack(tempUnit, 2);
                    UIController.inst.ShowPopupHint("You take 2 Damage");
                }
                else
                {
                    //Quest choice result 2
                    Debug.Log("Quest 1 Event choice 2");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.PoisonTarget,null, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer],0,1);
                    UIController.inst.ShowPopupHint("You take POISON");
                }
                break;
            case 2:
                if (choice == 0)
                {
                    //Quest choice result 0
                    Debug.Log("Quest 2 Event choice 0");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.AddCardToDeck, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 19);
                    UIController.inst.ShowPopupHint("You got 1 Dark Spirit");
                }
                else if (choice == 1)
                {
                    //Quest choice result 1
                    Debug.Log("Quest 2 Event choice 1");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.BuffAtkSelf, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null,1,5);
                    UIController.inst.ShowPopupHint("You got STRENGTH for 5 turns");
                }
                break;
            case 3:
                if (choice == 0)
                {
                    //Quest choice result 0
                    Debug.Log("Quest 3 Event choice 0");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.AddCardToDeck, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 28);
                    UIController.inst.ShowPopupHint("You got 1 Palladium Sword card");
                }
                else if (choice == 1)
                {
                    //Quest choice result 1
                    Debug.Log("Quest 3 Event choice 1");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.AddCardToDeck, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 29);
                    UIController.inst.ShowPopupHint("You got 1 Palladium Shield card");
                }
                else
                {
                    //Quest choice result 2
                    Debug.Log("Quest 3 Event choice 2");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.AddCardToDeck, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 30);
                    UIController.inst.ShowPopupHint("You got 1 Palladium Armour card");
                }
                break;
            case 4:
                if (choice == 0)
                {
                    //Quest choice result 0
                    Debug.Log("Quest 4 Event choice 0");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.InsightSelf, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 0, 2);
                    UIController.inst.ShowPopupHint("You got INSIGHT for 5 turns");
                }
                else if (choice == 1)
                {
                    //Quest choice result 1
                    Debug.Log("Quest 4 Event choice 1");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.BarrierSelf, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 0, 1);
                    UIController.inst.ShowPopupHint("You got BARRIER for 5 turns");
                }
                else
                {
                    //Quest choice result 2
                    Debug.Log("Quest 4 Event choice 2");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.SwiftSelf, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 0, 2);
                    UIController.inst.ShowPopupHint("You got SWIFT for 5 turns");
                }
                break;
        }
        TurnBoardController.inst.EndPhase();
    }

    public void SelectQuestEvent(int id, int choice)
    {
        Unit tempUnit = new Unit();
        switch (id)
        {
            case 0:
                if (choice == 0)
                {
                    //Quest choice result 0
                    Debug.Log("Quest 0 Event choice 0");
                    CardController.inst.MoveDiscardToPile(GameController.inst.CurrentPlayer);
                    UIController.inst.ShowPopupHint("Move all discard pile to the deck pile");
                }
                else
                {
                    //Quest choice result 1
                    Debug.Log("Quest 0 Event choice 1");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.AddCardToDeck, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 19);
                    UIController.inst.ShowPopupHint("Add 1 Dark Spirit card");
                }
                break;
            case 1:
                if (choice == 0)
                {
                    //Quest choice result 0
                    Debug.Log("Quest 1 Event choice 0");
                    CardController.inst.RemoveCardFromHand(GameController.inst.CurrentPlayer);
                    CardController.inst.DrawCard(GameController.inst.CurrentPlayer,4);
                    UIController.inst.ShowPopupHint("Redraw new cards [cost no AP]");
                }
                else
                {
                    //Quest choice result 1
                    Debug.Log("Quest 1 Event choice 1");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.WantedTarget, null,BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer],0, 1);
                    UIController.inst.ShowPopupHint("Apply WANTED for 1 turn");
                }
                break;
            case 2:
                if (choice == 0)
                {
                    //Quest choice result 0\
                    //need check !!!
                    Debug.Log("Quest 2 Event choice 0");
                    BoardController.inst.MovePiece(GameController.inst.CurrentPlayer, BoardController.inst.GetNearestMonster(GameController.inst.CurrentPlayer));
                    UIController.inst.ShowPopupHint("Move the player to a nearest monster and get a result");
                }
                else
                {
                    //Quest choice result 1
                    Debug.Log("Quest 2 Event choice 1");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.WantedTarget, null, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], 0, 2);
                    UIController.inst.ShowPopupHint("Apply WANTED for 2 turns");
                }
                break;
            case 3:
                if (choice == 0)
                {
                    //Quest choice result 0\
                    Debug.Log("Quest 3 Event choice 0");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.AddCardToDeck, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 21);
                    UIController.inst.ShowPopupHint("Add 1 Holy Water card");
                }
                else
                {
                    //Quest choice result 1
                    Debug.Log("Quest 3 Event choice 1");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.RecoverHpSelf, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 4, 0);
                    UIController.inst.ShowPopupHint("Heal 4 HP");
                }
                break;
            case 4:
                if (choice == 0)
                {
                    //Quest choice result 0\
                    Debug.Log("Quest 4 Event choice 0");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.AddCardToDeck, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 23);
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.WantedTarget, null,BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer],  0,1);
                    UIController.inst.ShowPopupHint("Add 1 Supply Wagon card, Apply Wanted for 1 turn");
                }
                else
                {
                    //Quest choice result 1
                    Debug.Log("Quest 4 Event choice 1");
                    UIController.inst.ShowPopupHint("Nothing Happend");
                }
                break;
            case 5:
                if (choice == 0)
                {
                    //Quest choice result 0\
                    Debug.Log("Quest 5 Event choice 0");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.AddCardToDeck, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 27);
                    UIController.inst.ShowPopupHint("Add 1 Dark Power card");
                }
                else
                {
                    //Quest choice result 1
                    Debug.Log("Quest 5 Event choice 1");
                    UIController.inst.ShowPopupHint("Nothing Happend");
                }
                break;
            case 6:
                if (choice == 0)
                {
                    //Quest choice result 0\
                    Debug.Log("Quest 6 Event choice 0");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.AddCardToDeck, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 24);
                    UIController.inst.ShowPopupHint("Add 1 Sword of Light card");
                }
                else
                {
                    //Quest choice result 1
                    Debug.Log("Quest 6 Event choice 1");
                    UIController.inst.ShowPopupHint("Nothing Happend");
                }
                break;
            case 7:
                if (choice == 0)
                {
                    //Quest choice result 0\
                    Debug.Log("Quest 7 Event choice 0");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.AddCardToDeck, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 26);
                    UIController.inst.ShowPopupHint("Add 1 Cursed Blade card");
                }
                else
                {
                    //Quest choice result 1
                    Debug.Log("Quest 7 Event choice 1");
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.AddCardToDeck, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 20);
                    EffectController.inst.ActiveEffect(CardEffect.EffectType.AddCardToDeck, BoardController.inst.playerPieceList[GameController.inst.CurrentPlayer], null, 20);
                    UIController.inst.ShowPopupHint("Add 2 Panic card");
                }
                break;
        }
        TurnBoardController.inst.EndPhase();
    }
}
