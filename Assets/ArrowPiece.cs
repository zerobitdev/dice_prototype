﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowPiece : MonoBehaviour {
    public Image arrow;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void showArrowOnUnit(GameObject _obj,Color _color)
    {
        arrow.color = _color;
        Vector3 pos = new Vector3(_obj.transform.position.x, _obj.transform.position.y + 2.1f,_obj.transform.position.z);
        transform.position = pos;

    }
}
